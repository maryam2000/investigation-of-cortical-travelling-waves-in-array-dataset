# Investigation of Cortical Travelling Waves in Array Dataset

- This is my Advance Topics and Computational Neuroscience Course Simulation 04

- Analysed activity of Local Field Potential signal recorded by multi-electrode array in the premotor area F5 (Hatsopoulos et.al 2006).
- Clustered channels based on their dominant frequency after color noise cancellation.
- Used Hilbert transform to calculate instantaneous phase of filtered LFP signals (Butterworth filtering), then applied Phase Gradient Directionality to figure out the propagation direction of the travelling waves.

- The uploaded file contains codes and the project report.
